﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HomeWork4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public class Car
    {
        public string text1 { get; set; }
        public string text2 { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
    }
    

    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Car> CarList { get; private set; }


        public MainPage()
        {
            InitializeComponent();
            CarList = new ObservableCollection<Car>();



            CarList.Add(new Car
            {
                text1 = "TOYOTA Supra",
                text2 = "I would like to buy one but it's expensive",
                Img1 = "supra.jpg",
                Img2 = "supra2.jpg"
            });

            CarList.Add(new Car
            {
                text1 = "Renault Megane 3 RS",
                text2 = "I would like a red one",
                Img1 = "m3rs.jpg",
                Img2 = "m3rs2.jpg"
            });

            CarList.Add(new Car
            {
                text1 = "Subaru impreza wrx sti",
                text2 = "Awesome engine",
                Img1 = "sub.jpg",
                Img2 = "sub2.jpg"
            });
            
            CarList.Add(new Car
            {
                text1 = "Toyota GT86",
                text2 = "The GT86 is like a mini supercar",
                Img1 = "gt86.jpg",
                Img2 = "gt862.jpg"
            });


            BindingContext = this;
        }

        private void OnDelete(object sender, EventArgs e)
        {
            var listItem = ((MenuItem)sender);
            var car = (Car)listItem.CommandParameter;
            DisplayAlert("I SEE YOU", car.text1, "OK");

            CarList.Remove(car);
        }

       private async void OnOpen(Object sender, EventArgs e)
        {
            var listItem = ((MenuItem)sender);
            var car = (Car)listItem.CommandParameter;
            if (car.text1 == "TOYOTA Supra")
                await Navigation.PushAsync(new Page1());
            if (car.text1 == "Renault Megane 3 RS")
                await Navigation.PushAsync(new Page2());
            if (car.text1 == "Subaru impreza wrx sti")
                await Navigation.PushAsync(new Page3());
            if (car.text1 == "Toyota GT86")
                await Navigation.PushAsync(new Page4());
        }
    }
}
